import React from "react";
import "./RoomeHomepage.css";

class RoomeHomepage extends React.Component {
  constructor() {
    super();
    this.state = {
      lookingFor: '',
      occupants: '',
      postcodeOne: '',
      postcodeTwo: '',
      postcodeThree: '',
      postcodeFour: '',
      postcodeFive: '',
      noOfRooms: '',
      priceRange: '',
    };
  }

  Hello = () => {
    alert('Login In Construction Mode');
  }
  SearchHere = () => {
    alert('Search Box In Construction Mode');
  }
  setLookingFor = (e) => {
    this.setState({lookingFor: e.target.value});
  }
  setOccupants = (e) => {
    this.setState({occupants: e.target.value});
  }
  setPostcode = (e) => {
    this.setState({[e.target.name]:e.target.value});
  }
  setNoOfRooms = (e) => {
    this.setState({noOfRooms: e.target.value});
  }
  setPriceRange = (e) => {
    this.setState({priceRange: e.target.value});
  }
  seeResults = () => {
    document.getElementById('id01').style.display='block';
  }
  closeModal = () => {
    document.getElementById('id01').style.display='none';
  }
  render() {
    return (
      <div className="roome-homepage">
        <div className="rectangle-div" />
        <div className="rectangle-div1" />
        <img className="vector-icon" alt="" src="../vector.svg" />
        <div className="rectangle-div2" />
        <div className="rectangle-div3" />
        <img className="image-1-icon" alt="" src="../image-1@2x.png" />
        <div className="group-div">
          <div className="rectangle-div4" />
          <div className="rectangle-div5" />
          <div className="rectangle-div6" />
        </div>
        <div className="group-div1">
          <div className="rectangle-div7" />
          <div className="new-refurbished-apartment">
            <p className="new-p">NEW!</p>
            <p className="refurbished-apartment">Refurbished Apartment</p>
          </div>
          <div className="unoccupied-ready-in-feburary">
            Unoccupied | Ready in Feburary
          </div>
          <div className="added-on-the-110222">Added on the 11/02/22</div>
          <div className="sign-in-to-learn-more-and-cont">
            Sign in to learn more and contact agent
          </div>
          <div className="pw-div">£210pw</div>
          <div className="group-div2">
            <div className="div">4</div>
            <img className="group-icon" alt="" src="../group.svg" />
          </div>
          <div className="group-div3">
            <div className="rectangle-div8" />
            <div className="view-more">{`View More >`}</div>
          </div>
          <div className="rectangle-div9" />
          <div className="bedroom-with-en-suite-now-avai">
            Bedroom with en-suite now available in Guildford
          </div>
          <div className="occupied-ready-in-april">Occupied | Ready in April</div>
          <div className="added-on-the-070222">Added on the 07/02/22</div>
          <div className="sign-in-to-learn-more-and-cont1">
            Sign in to learn more and contact agent.
          </div>
          <div className="pw-div1">£210pw</div>
          <div className="group-div4">
            <div className="div">6</div>
            <img className="group-icon1" alt="" src="../group1.svg" />
          </div>
          <div className="group-div5">
            <div className="rectangle-div8" />
            <div className="view-more">{`View More >`}</div>
          </div>
          <div className="rectangle-div11" />
          <div className="wanted-roommate-for-all-femal">
            WANTED! Roommate for all female flat.
          </div>
          <div className="occupied-ready-to-move-in">
            Occupied | Ready to move in!
          </div>
          <div className="added-on-the-090222">Added on the 09/02/22</div>
          <div className="sign-in-to-learn-more-and-cont2">
            Sign in to learn more and contact agent.
          </div>
          <div className="pw-div2">£220pw</div>
          <div className="group-div6">
            <div className="div">4</div>
            <img className="group-icon" alt="" src="../group1.svg" />
          </div>
          <div className="group-div7">
            <div className="rectangle-div8" />
            <div className="view-more">{`View More >`}</div>
          </div>
          <img className="rectangle-icon" alt="" src="../rectangle@2x.png" />
          <div className="rectangle-div13" />
          <img className="group-icon3" alt="" src="../group-41.svg" />
          <img className="group-icon4" alt="" src="../group3.svg" />
          <div className="just-landed-div">
            <p className="new-p">Just</p>
            <p className="refurbished-apartment">Landed!</p>
          </div>
          <img className="rectangle-icon1" alt="" src="../rectangle1@2x.png" />
          <div className="rectangle-div14" />
          <img className="group-icon5" alt="" src="../group-41.svg" />
          <img className="group-icon6" alt="" src="../group4.svg" />
          <div className="premium-property-div">
            <p className="new-p">Premium</p>
            <p className="refurbished-apartment">Property</p>
          </div>
          <img className="rectangle-icon2" alt="" src="../rectangle2@2x.png" />
          <div className="rectangle-div15" />
          <img className="group-icon7" alt="" src="../group-41.svg" />
          <img className="group-icon8" alt="" src="../group5.svg" />
          <div className="just-landed-div1">
            <p className="new-p">Just</p>
            <p className="refurbished-apartment">Landed!</p>
          </div>
          <img className="vector-icon1" alt="" src="../vector1.svg" />
          <img className="vector-icon2" alt="" src="../vector1.svg" />
          <img className="vector-icon3" alt="" src="../vector1.svg" />
        </div>
        <div className="rectangle-div16" />
        <div className="bed-property-in-guildford">
          5 Bed Property in Guildford
        </div>
        <div className="unoccupied-ready-to-move-in">
          Unoccupied | Ready to move in!
        </div>
        <div className="added-on-the-100222">Added on the 10/02/22</div>
        <div className="sign-in-to-learn-more-and-cont3">
          Sign in to learn more and contact agent.
        </div>
        <div className="pw-div3">£230pw</div>
        <div className="div3">5</div>
        <img className="group-icon9" alt="" src="../group.svg" />
        <div className="group-div8">
          <div className="rectangle-div8" />
          <div className="view-more">{`View More >`}</div>
        </div>
        <div className="rectangle-div18" />
        <div className="rectangle-div19" />
        <img className="vector-icon4" alt="" src="../vector1.svg" />
        <div className="room-to-let-farnham-rd">Room to let Farnham Rd</div>
        <div className="bed-flat-in-woodbridge">3 Bed Flat in Woodbridge</div>
        <div className="occupied-ready-in-april1">Occupied | Ready in April</div>
        <div className="unoccupied-ready-to-move-in1">
          Unoccupied | Ready to move in!
        </div>
        <div className="added-on-the-1102221">Added on the 11/02/22</div>
        <div className="added-on-the-0902221">Added on the 09/02/22</div>
        <div className="sign-in-to-learn-more-and-cont4">
          Sign in to learn more and contact agent
        </div>
        <div className="sign-in-to-learn-more-and-cont5">
          Sign in to learn more and contact agent.
        </div>
        <div className="pw-div4">£210pw</div>
        <div className="pw-div5">£250pw</div>
        <div className="div4">6</div>
        <div className="div5">3</div>
        <img className="group-icon10" alt="" src="../group1.svg" />
        <img className="group-icon11" alt="" src="../group1.svg" />
        <div className="group-div9">
          <div className="rectangle-div8" />
          <div className="view-more">{`View More >`}</div>
        </div>
        <div className="group-div10">
          <div className="rectangle-div8" />
          <div className="view-more">{`View More >`}</div>
        </div>
        <img className="vector-icon5" alt="" src="../vector1.svg" />
        <img className="vector-icon6" alt="" src="../vector1.svg" />
        <div className="featured-properties-div">Featured Properties</div>
        <div className="advertise-on-roome">Advertise on Roome</div>
        <div className="visual-search-div">Visual Search</div>
        <div className="sign-up-today">Sign up today!</div>
        <div className="check-these-out-while-theyre">
          Check these out while they’re still around!
        </div>
        <div className="blogs-div">Blogs</div>
        <div className="free-for-students-roome-is-th">
          Free for students, Roome is the ideal place to reach the right people
        </div>
        <div className="never-miss-a-update-listing-o">
          Never miss a update, listing or message again.
        </div>
        <div className="already-a-member-sign-in">
          {`Already a member? `}
          <span className="sign-in-span">Sign In.</span>
        </div>
        <div className="showing-results-in-the-surrey">
          {`Showing results in the Surrey Area. `}
          <span className="sign-in-span">Change Location?</span>
        </div>
        <div className="showing-results-in-the-surrey1">
          {`Showing results in the Surrey Area. `}
          <span className="sign-in-span">Change Location?</span>
        </div>
        <div className="rectangle-div22" />
        <div className="group-div11">
          <div className="rectangle-div23" />
          <div className="register-as-a-student">Register as a Student</div>
          <div className="rectangle-div24" />
          <div className="advertise-on-roome1">Advertise on Roome</div>
        </div>
        <div className="group-div12">
          <div className="group-div13">
            <div className="rectangle-div25" />
            <div className="how-will-energy-price-increase">
              How will energy price increases effect you?
            </div>
            <div className="lorem-ipsum-dolor-sit-amet-co">
              “Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
              nonummy nibh euismod tincidunt exerci tation ullamcorper suscipit
              lobortis nisl ut aliquip ex ea commodo ...”
            </div>
            <div className="john-smith-div">John Smith</div>
            <div className="div6">24/02/22</div>
            <img className="group-icon12" alt="" src="../group9.svg" />
            <div className="group-div14">
              <div className="rectangle-div8" />
              <div className="view-more">{`View More >`}</div>
            </div>
            <img className="group-icon13" alt="" src="../group10.svg" />
            <div className="rectangle-div27" />
            <div className="rectangle-div28" />
            <img className="vector-icon7" alt="" src="../vector-19.svg" />
            <img className="rectangle-icon3" alt="" src="../rectangle3@2x.png" />
            <div className="rectangle-div29" />
            <div className="bills-finance">{`Bills & Finance`}</div>
            <div className="labels-on-the-milk">Labels on the Milk?</div>
            <div className="insurance-what-kind-do-you-ne">
              Insurance? What kind do you need?
            </div>
            <div className="lorem-ipsum-dolor-sit-amet-co1">
              “Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
              nonummy nibh euismod tincidunt exerci tation ullamcorper suscipit
              lobortis nisl ut aliquip ex ea commodo ...”
            </div>
            <div className="lorem-ipsum-dolor-sit-amet-co2">
              “Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
              nonummy nibh euismod tincidunt exerci tation ullamcorper suscipit
              lobortis nisl ut aliquip ex ea commodo ...”
            </div>
            <div className="sarah-jones-div">Sarah Jones</div>
            <div className="john-smith-div1">John Smith</div>
            <div className="div7">20/02/22</div>
            <div className="div8">18/02/22</div>
            <img className="group-icon14" alt="" src="../group9.svg" />
            <img className="group-icon15" alt="" src="../group9.svg" />
            <div className="group-div15">
              <div className="rectangle-div8" />
              <div className="view-more">{`View More >`}</div>
            </div>
            <div className="group-div16">
              <div className="rectangle-div8" />
              <div className="view-more">{`View More >`}</div>
            </div>
            <img className="group-icon16" alt="" src="../group13.svg" />
            <img className="group-icon17" alt="" src="../group10.svg" />
            <img className="vector-icon8" alt="" src="../vector-19.svg" />
            <img className="vector-icon9" alt="" src="../vector-19.svg" />
            <img className="rectangle-icon4" alt="" src="../rectangle4@2x.png" />
            <div className="rectangle-div32" />
            <div className="day-to-day">Day to Day</div>
            <img className="rectangle-icon5" alt="" src="../rectangle5@2x.png" />
            <div className="rectangle-div33" />
            <div className="bills-finance1">{`Bills & Finance`}</div>
          </div>
        </div>
        <img className="rectangle-icon6" alt="" src="../rectangle6@2x.png" />
        <div className="group-div17">
          <img className="group-icon18" alt="" src="../group15.svg" />
          <div className="premium-property-div1">
            <p className="new-p">Premium</p>
            <p className="refurbished-apartment">Property</p>
          </div>
        </div>
        <div className="rectangle-div34" />
        <img className="group-icon19" alt="" src="../group-41.svg" />
        <img className="rectangle-icon7" alt="" src="../rectangle7@2x.png" />
        <div className="rectangle-div35" />
        <img className="group-icon20" alt="" src="../group-41.svg" />
        <div className="group-div18">
          <img className="group-icon18" alt="" src="../group16.svg" />
          <div className="just-landed-div2">
            <p className="new-p">Just</p>
            <p className="refurbished-apartment">Landed!</p>
          </div>
        </div>
        <img className="rectangle-icon8" alt="" src="../rectangle8@2x.png" />
        <div className="rectangle-div36" />
        <img className="group-icon22" alt="" src="../group-41.svg" />
        <div className="group-div19">
          <img className="group-icon18" alt="" src="../group17.svg" />
          <div className="premium-property-div1">
            <p className="new-p">Premium</p>
            <p className="refurbished-apartment">Property</p>
          </div>
        </div>
        <img className="group-icon24" alt="" src="../group-360.svg" />
        <img className="rectangle-icon9" alt="" src="../rectangle9@2x.png" />
        <div className="group-div20">
          <div className="rectangle-div37" />
          <div className="get-the-app">Get the app!</div>
          <img className="image-2-icon" alt="" src="../image-2@2x.png" />
          <div className="group-div21">
            <div className="contact-div">Contact</div>
            <img className="vector-icon10" alt="" src="../vector-22.svg" />
            <div className="faq-div">FAQ</div>
            <img className="vector-icon11" alt="" src="../vector-22.svg" />
            <div className="blogs-guides">{`Blogs & Guides`}</div>
            <img className="vector-icon12" alt="" src="../vector-22.svg" />
            <div className="insurance-div">Insurance</div>
            <img className="vector-icon13" alt="" src="../vector-22.svg" />
            <div className="terms-conditions">{`Terms & Conditions`}</div>
            <img className="vector-icon14" alt="" src="../vector-22.svg" />
            <div className="privacy-policy-div">Privacy Policy</div>
            <img className="vector-icon15" alt="" src="../vector-22.svg" />
            <div className="code-of-practice-for-agents">
              Code of Practice for Agents
            </div>
            <img className="vector-icon16" alt="" src="../vector-22.svg" />
            <div className="discrimination-div">Discrimination</div>
            <div className="jobs-div">Jobs</div>
            <img className="vector-icon17" alt="" src="../vector-22.svg" />
            <img className="vector-icon18" alt="" src="../vector-22.svg" />
          </div>
          <div className="group-div22">
            <div className="advertise-a-listing">- Advertise a Listing</div>
            <div className="find-a-room">- Find a Room</div>
            <div className="create-a-account">- Create a account</div>
            <div className="plans-subscriptions">{`-  Plans & Subscriptions`}</div>
          </div>
          <img className="vector-icon19" alt="" src="../vector-31.svg" />
          <div className="lorem-ipsum-is-a-dummy-text">
            Lorem ipsum is a dummy text!
          </div>
          <img className="rectangle-icon10" alt="" src="../rectangle10@2x.png" />
        </div>
        <img className="group-icon25" alt="" src="../group18.svg" />
        <img className="mask-group-icon" alt="" src="../mask-group@2x.png" />
        <div className="rectangle-div38" />
        <img className="rectangle-icon11" alt="" src="../rectangle11@2x.png" />
        <div className="advertise-spare-rooms-search">
          Advertise spare rooms, search for housemates and find student homes on
          our mobile and web app.
        </div>
        <div className="rectangle-div39" />
        <div className="group-div23">
          <div className="create-a-listing">Create a Listing</div>
          <img className="icon" alt="" src="../1.svg" />
        </div>
        <div className="rectangle-div40" />
        <div className="frame-div">
          <div className="find-a-room1">Find a Room</div>
          <img className="icon1" alt="" src="../1.svg" />
        </div>
        <div className="group-div24">
          {/* <div className="rectangle-div41" /> */}
          <button onClick={this.Hello} className="rectangle-div41">Login</button>
        </div>
        <div className="group-div25">
          <img className="vector-icon20" alt="" src="../vector7.svg" />
          <img className="vector-icon21" alt="" src="../vector8.svg" />
          <div className="landlords-div">Landlords</div>
          <div className="students-div">Students</div>
          <div className="start-your-7-day-free-trial">{`Start your 7 day free trial `}</div>
          <div className="start-your-7-day-free-trial1">{`Start your 7 day free trial `}</div>
          <div className="start-your-7-day-free-trial2">
            Start your 7 day free trial
          </div>
          <div className="div9">£25</div>
          <div className="div10">£15</div>
          <div className="group-div26">
            <div className="advertise-quickly-and-easily">
              Advertise quickly and easily
            </div>
            <div className="communicate-easily-div">Communicate easily</div>
            <div className="review-booking-requests">Review booking requests</div>
            <div className="keep-your-information-private">
              Keep your information private
            </div>
            <div className="booking-system-div">Booking System</div>
            <div className="we-do-all-the-hard-work">
              We do all the hard work!
            </div>
            <img className="vector-icon22" alt="" src="../vector-10.svg" />
            <img className="vector-icon23" alt="" src="../vector-10.svg" />
            <img className="vector-icon24" alt="" src="../vector-10.svg" />
            <img className="vector-icon25" alt="" src="../vector-10.svg" />
            <img className="vector-icon26" alt="" src="../vector-10.svg" />
            <img className="vector-icon27" alt="" src="../vector-10.svg" />
          </div>
          <div className="group-div27">
            <div className="advertise-quickly-and-easily">
              Advertise quickly and easily
            </div>
            <div className="communicate-easily-div">Communicate easily</div>
            <div className="review-booking-requests">
              We do all the hard work!
            </div>
            <img className="vector-icon22" alt="" src="../vector-101.svg" />
            <img className="vector-icon23" alt="" src="../vector-101.svg" />
            <img className="vector-icon25" alt="" src="../vector-101.svg" />
          </div>
          <img className="vector-icon31" alt="" src="../vector8.svg" />
          <div className="letting-agents-div">Letting Agents</div>
          <div className="contact-our-team-today">{`Contact our team today `}</div>
          <div className="enquire-today-div">Enquire Today</div>
          <div className="group-div28">
            <div className="advertise-quickly-and-easily">
              Advertise quickly and easily
            </div>
            <div className="communicate-easily-div">Communicate easily</div>
            <div className="review-booking-requests">Review booking requests</div>
            <div className="keep-your-information-private">{`Built in message reports `}</div>
            <div className="designated-page-for-your-prope">
              Designated page for your properties
            </div>
            <div className="sync-your-property-portfolio">
              Sync your property portfolio
            </div>
            <div className="crm-div">{`CRM `}</div>
            <div className="booking-system-div1">Booking System</div>
            <div className="we-do-all-the-hard-work2">
              We do all the hard work!
            </div>
            <img className="vector-icon22" alt="" src="../vector-101.svg" />
            <img className="vector-icon23" alt="" src="../vector-101.svg" />
            <img className="vector-icon25" alt="" src="../vector-101.svg" />
            <img className="vector-icon24" alt="" src="../vector-101.svg" />
            <img className="vector-icon36" alt="" src="../vector-101.svg" />
            <img className="vector-icon37" alt="" src="../vector-101.svg" />
            <img className="vector-icon38" alt="" src="../vector-101.svg" />
            <img className="vector-icon39" alt="" src="../vector-101.svg" />
            <img className="vector-icon40" alt="" src="../vector-101.svg" />
          </div>
        </div>
        <div className="group-div29">
          <div className="rectangle-div42" />
          <div className="start-your-free-trial">Start Your Free Trial</div>
        </div>
        <div className="see-plan-options">See Plan Options</div>
        <img className="vector-icon41" alt="" src="../vector-72.svg" />
        <img className="group-icon26" alt="" src="../group-367.svg" />
        <div className="group-div30">
          <img className="group-icon18" alt="" src="../vector10.svg" />
          <img className="group-icon27" alt="" src="../group19.svg" />
          <img className="group-icon28" alt="" src="../group20.svg" />
          <img className="group-icon29" alt="" src="../group20.svg" />
          <img className="group-icon30" alt="" src="../group20.svg" />
          <img className="group-icon31" alt="" src="../group20.svg" />
          <img className="group-icon32" alt="" src="../group20.svg" />
          <img className="group-icon33" alt="" src="../group20.svg" />
          <img className="group-icon34" alt="" src="../group20.svg" />
          <img className="group-icon35" alt="" src="../group20.svg" />
          <img className="group-icon36" alt="" src="../group20.svg" />
          <img className="group-icon37" alt="" src="../group20.svg" />
          <img className="group-icon38" alt="" src="../group20.svg" />
          <img className="group-icon39" alt="" src="../group20.svg" />
          <img className="group-icon40" alt="" src="../group20.svg" />
          <img className="ellipse-icon" alt="" src="../ellipse-41.svg" />
          <img className="group-icon41" alt="" src="../group33.svg" />
          <img className="group-icon42" alt="" src="../group34.svg" />
          <div className="union-div">
            <div className="rectangle-div43" />
            <div className="rectangle-div44" />
          </div>
          <div className="div11">4</div>
          <div className="div12">£215</div>
          <div className="pp-div">PP</div>
          <img className="group-icon43" alt="" src="../group35.svg" />
        </div>
        <div className="group-div31">
          <div className="find-a-roome">Find a roome</div>
          <div className="search-by-university-or-postco">
            Search by university or postcode
          </div>
          <div className="quick-search-div">Quick Search</div>
          <div className="advanced-search-div">Advanced Search</div>
          <div className="group-div32">
            <button onClick={this.seeResults} className="rectangle-div45">
              See Results
            </button>
            {/* <div className="rectangle-div45" />
            <div className="see-results-div">See Results</div> */}
          </div>
          <div className="group-div33">
            <div className="rectangle-div46" />
            <button onClick={this.SearchHere} className="rectangle-div47" />
            <input type='text' placeholder="Search Here ..." className="search-here"/>
            <img className="group-icon44" alt="" src="../group-9.svg" />
            <img className="frame-icon" alt="" src="../frame.svg" />
          </div>
          <img className="vector-icon43" alt="" src="../vector-72.svg" />
          <img className="ff-icon" alt="" src="../000000ff.svg" />
          <div className="i-am-a-looking-for">I am a looking for</div>
          <button onClick={this.setLookingFor} value="A Room" className="rectangle-div48">
            A Room
          </button>
          <button onClick={this.setLookingFor} value="A House / Flat" className="rectangle-div49">
            A House / Flat
          </button>
          <button onClick={this.setLookingFor} value="A Roommate" className="rectangle-div50">
            A Roommate
          </button>
          <div className="occupants-div">Occupants</div>
          <div className="number-of-rooms">Number of Rooms</div>
          <div className="postcode-area-div">Postcode Area</div>
          <button onClick={this.setOccupants} value="01" className="rectangle-div51">
            01
          </button>
          <button onClick={this.setNoOfRooms} value="01" className="rectangle-div52">
            01
          </button>
          <button onClick={this.setOccupants} value="02" className="rectangle-div53">
            02
          </button>
          <button onClick={this.setNoOfRooms} value="08" className="rectangle-div54">
            08
          </button>
          <button onClick={this.setOccupants} value="03" className="rectangle-div55">
            03
          </button>
          <button onClick={this.setNoOfRooms} value="03" className="rectangle-div56">
            03
          </button>
          <button onClick={this.setNoOfRooms} value="02" className="rectangle-div57">
            02
          </button>
          <button onClick={this.setOccupants} value="04" className="rectangle-div58">
            04
          </button>
          <button onClick={this.setNoOfRooms} value="04" className="rectangle-div59">
            04
          </button>
          <button onClick={this.setOccupants} value="05" className="rectangle-div60">
            05
          </button>
          <button onClick={this.setNoOfRooms} value="05" className="rectangle-div61">
            05
          </button>
          <button onClick={this.setOccupants} value="06" className="rectangle-div62">
            06
          </button>
          <button onClick={this.setNoOfRooms} value="06" className="rectangle-div63">
            06
          </button>
          <button onClick={this.setOccupants} value="07" className="rectangle-div64">
            07
          </button>
          <button onClick={this.setNoOfRooms} value="07" className="rectangle-div65">
            07
          </button>
          <button onClick={this.setOccupants} value="08" className="rectangle-div66">
            08
          </button>
          <button onClick={this.setOccupants} value="09" className="rectangle-div67">
            09
          </button>
          <button onClick={this.setNoOfRooms} value="09" className="rectangle-div68">
            09
          </button>
          <button onClick={this.setOccupants} value="10" className="rectangle-div69">
            10
          </button>
          <button onClick={this.setNoOfRooms} value="10" className="rectangle-div70">
            10
          </button>
          {/* <div className="div13">03</div> */}
          {/* <div className="div14">03</div> */}
          {/* <div className="div15">02</div> */}
          {/* <div className="div16">01</div> */}
          {/* <div className="div17">01</div> */}
          {/* <div className="div18">04</div> */}
          {/* <div className="div19">04</div> */}
          {/* <div className="div20">02</div> */}
          {/* <div className="div21">08</div> */}
          {/* <div className="div22">05</div> */}
          {/* <div className="div23">05</div> */}
          {/* <div className="rectangle-div71" /> */}
          {/* <input type='text' className="rectangle-div71"/> */}
          <input type="text" onChange={this.setPostcode} name="postcodeOne" maxLength="1" className="rectangle-div71"/>
          <input type="text" onChange={this.setPostcode} name="postcodeTwo" maxLength="1" className="rectangle-div72"/>
          <input type="text" onChange={this.setPostcode} name="postcodeThree" maxLength="1" className="rectangle-div73"/>
          <input type="text" onChange={this.setPostcode} name="postcodeFour" maxLength="1" className="rectangle-div74"/>
          <input type="text" onChange={this.setPostcode} name="postcodeFive" maxLength="1" className="rectangle-div75"/>
          {/* <div className="div24">06</div> */}
          {/* <div className="div25">06</div> */}
          {/* <div className="div26">07</div> */}
          {/* <div className="div27">07</div> */}
          {/* <div className="div28">08</div> */}
          {/* <div className="div29">09</div> */}
          {/* <div className="div30">09</div> */}
          {/* <div className="div31">10</div> */}
          {/* <div className="div32">10</div> */}
          <div className="price-range-div">Price Range</div>
          <button onClick={this.setPriceRange} value="$01 - $100" className="rectangle-div76">
          $01 - $100
          </button>
          <button onClick={this.setPriceRange} value="$100 - $250" className="rectangle-div77">
            $100 - $250
          </button>
          <button onClick={this.setPriceRange} value="$250 - $400" className="rectangle-div78">
            $250 - $400
          </button>
          <button onClick={this.setPriceRange} value="$400 - $600" className="rectangle-div79">
            $400 - $600
          </button>
          {/* <div className="div33">{`$01 - $100  `}</div>
          <div className="div34">{`$100 - $250  `}</div>
          <div className="div35">{`$250 - $400  `}</div>
          <div className="div36">{`$400 - $600  `}</div> */}
        </div>
        <img className="rectangle-icon12" alt="" src="../rectangle12@2x.png" />
        <img className="rectangle-icon13" alt="" src="../rectangle13@2x.png" />
        <img className="group-icon45" alt="" src="../group36.svg" />
        <img className="rectangle-icon14" alt="" src="../rectangle14@2x.png" />
        <div id="id01" className="w3-modal">
          <div className="w3-modal-content">
            <div className="w3-container">
              <span onClick={this.closeModal} className="w3-button w3-display-topright">&times;</span>
              <p style={{fontSize: '20px',color: 'black',textAlign: 'center'}}><u><b>Results</b></u></p>
              <p style={{fontSize: '20px',color: 'black',textAlign: 'center'}}>You are looking for <b>{this.state.lookingFor}</b></p>
              <p style={{fontSize: '20px',color: 'black',textAlign: 'center'}}>Occupants are <b>{this.state.occupants}</b></p>
              <p style={{fontSize: '20px',color: 'black',textAlign: 'center'}}>Postcode Area are <b>{this.state.postcodeOne+this.state.postcodeTwo+this.state.postcodeThree+this.state.postcodeFour+this.state.postcodeFive}</b></p>
              <p style={{fontSize: '20px',color: 'black',textAlign: 'center'}}>You need number of rooms <b>{this.state.noOfRooms}</b></p>
              <p style={{fontSize: '20px',color: 'black',textAlign: 'center'}}>Your price range <b>{this.state.priceRange}</b></p>
            </div>
          </div>
        </div>
      </div>
    );
  };
};

export default RoomeHomepage;
